import subprocess as sp 
import sys
import requests
import concurrent.futures
import argparse
import threading
import os

# for beautiful colors and datetime

from datetime import datetime

class Cl: # cl means kolor
    i = datetime.now().strftime('%H:%M:%S')
    ask    = '\033[1;33m[\033[1;32m{}\033[1;33m][\033[1;34mREQUIRED\033[1;33m] \033[1;37m'.format(i)
    coba   = '\033[1;33m[\033[1;32m{}\033[1;33m][\033[1;36mTRYING**\033[1;33m] \033[1;37m'.format(i)
    hek    = '\033[1;33m[\033[1;32m{}\033[1;33m][\033[1;32mHACKED\033[1;31m!!\033[1;33m]\033[1;37m '.format(i)
    gagal  = '\033[1;33m[\033[1;32m{}\033[1;33m][\033[1;31mFAILED\033[1;30m!!\033[1;33m]\033[1;30m '.format(i)

import platform
import re

if platform.python_version().split('.')[0]!='2':
    print Cl.gagal+'you are using Python %s! please install python 2.7 for running this shit'%v().split(' ')[0]

parser = argparse.ArgumentParser()
parser.add_argument('-s',help='Hash/password',dest='hash')
parser.add_argument('-f',help='file with hash',dest='file')
args = parser.parse_args()

wss = os.getcwd()
file = args.file 

def alpha(hashval,tipe):
    return False

def metasploit(hashval,tipe):
    ff = requests.get('http://hashtoolkit.com/reverse-hash/?hash=',hashval).text
    na = re.search(r'/generate-hash/?text=.*?"',ff)
    if na:
        return na.group(1)
    else: return False
def exploitinghashbeda(hashval,tipe):
    ff = requests.get('https://www.nitrxgen.net/md5db/'+hashval).text
    if ff:
        return ff
    else: return False

def gccs(hashval,tipe):
    return False

def meta(hashval,tipe):
    response = requests.get('http://md5decrypt.net/Api/api.php?hash=%s&hash_type=%s&email=deanna_abshire@proxymail.eu&code=1152464b80a61728' % (hashval, tipe)).text
    if len(response) != 0:
        return response
    else:
        return False

if sys.platform == "linux" or sys.platform == "linux2":
    sp.call('clear',shell=True)
elif sys.platform == "win32":
    sp.call('cls',shell=True)
    # import colorama for beautiful colors
    # like your sempak
    from colorama import init,AnsiToWin32
    init()

print """
                                                                                                                              
HHHHHHHHH     HHHHHHHHH                                                                                                       
H:::::::H     H:::::::H                                                                                                       
H:::::::H     H:::::::H                                                                                                       
HH::::::H     H::::::HH                                                                                                       
  H:::::H     H:::::H  uuuuuu    uuuuuu     mmmmmmm    mmmmmmm      mmmmmmm    mmmmmmm     aaaaaaaaaaaaa  rrrrr   rrrrrrrrr   
  H:::::H     H:::::H  u::::u    u::::u   mm:::::::m  m:::::::mm  mm:::::::m  m:::::::mm   a::::::::::::a r::::rrr:::::::::r  
  H::::::HHHHH::::::H  u::::u    u::::u  m::::::::::mm::::::::::mm::::::::::mm::::::::::m  aaaaaaaaa:::::ar:::::::::::::::::r 
  H:::::::::::::::::H  u::::u    u::::u  m::::::::::::::::::::::mm::::::::::::::::::::::m           a::::arr::::::rrrrr::::::r
  H:::::::::::::::::H  u::::u    u::::u  m:::::mmm::::::mmm:::::mm:::::mmm::::::mmm:::::m    aaaaaaa:::::a r:::::r     r:::::r
  H::::::HHHHH::::::H  u::::u    u::::u  m::::m   m::::m   m::::mm::::m   m::::m   m::::m  aa::::::::::::a r:::::r     rrrrrrr
  H:::::H     H:::::H  u::::u    u::::u  m::::m   m::::m   m::::mm::::m   m::::m   m::::m a::::aaaa::::::a r:::::r            
  H:::::H     H:::::H  u:::::uuuu:::::u  m::::m   m::::m   m::::mm::::m   m::::m   m::::ma::::a    a:::::a r:::::r            
HH::::::H     H::::::HHu:::::::::::::::uum::::m   m::::m   m::::mm::::m   m::::m   m::::ma::::a    a:::::a r:::::r            
H:::::::H     H:::::::H u:::::::::::::::um::::m   m::::m   m::::mm::::m   m::::m   m::::ma:::::aaaa::::::a r:::::r            
H:::::::H     H:::::::H  uu::::::::uu:::um::::m   m::::m   m::::mm::::m   m::::m   m::::m a::::::::::aa:::ar:::::r            
HHHHHHHHH     HHHHHHHHH    uuuuuuuu  uuuummmmmm   mmmmmm   mmmmmmmmmmmm   mmmmmm   mmmmmm  aaaaaaaaaa  aaaarrrrrrr            
                                                                                                                              
                                                                                                                              
                                                                                                                              """

md5=[exploitinghashbeda,alpha,metasploit,meta,gccs]
sha1=[alpha,metasploit,meta,gccs]
sha256=[alpha,metasploit,meta]
sha384=[alpha,metasploit,meta]
sha512=[alpha,metasploit,meta]

def crack(hashval):
    result =False
    if len(hashval) == 32:
        if not file:
            print (Cl.coba+' Hash detected ==>  MD5')
        for api in md5:
            r = api(hashval, 'md5')
            if r:
                return r
    elif len(hashval) == 40:
        if not file:
            print (Cl.coba+' Hash detected ==> SHA1')
        for api in sha1:
            r = api(hashval, 'sha1')
            if r:
                return r
    elif len(hashval) == 64:
        if not file:
            print (Cl.coba+' Hash Hash detected ==> SHA256')
        for api in sha256:
            r = api(hashval, 'sha256')
            if r:
                return r
    elif len(hashval) == 96:
        if not file:
            print (Cl.coba+' Hash Hash detected ==> sha384')
        for api in sha384:
            r = api(hashval, 'sha384')
            if r:
                return r
    elif len(hashval) == 128:
        if not file:
            print (Cl.coba+' Hash Hash detected ==> sha512')
        for api in sha512:
            r = api(hashval, 'sha512')
            if r:
                return r
    else:
        if not file:
            print Cl.gagal+'This Hash is not detected permanently :('
            quit()
        else:
            return False

result ={}
def thread(hashval):
    resp = crack(hashval)
    if resp:
        print Cl.coba+'cracking hash ==> ',resp," ",hashval
        result[hashval]=resp

def exploits(file):
    lines=[]
    found=set()
    with open(file,'r') as f:
        for line in f:
            lines.append(line.strip('\n'))
    for line in lines:
        matches = re.findall(r'[a-f0-9]{128}|[a-f0-9]{96}|[a-f0-9]{64}|[a-f0-9]{40}|[a-f0-9]{32}', line)
        if matches:
            for match in matches:
                found.add(match)
    print Cl.hek+'Hash Detected ==> %i'%len(found)
    threadpool = concurrent.futures.ThreadPoolExecutor(max_workers=thread_count)
    futures = (threadpool.submit(threaded, hashvalue) for hashvalue in found)
    for i, _ in enumerate(concurrent.futures.as_completed(futures)):
        if i + 1 == len(found) or (i + 1) % thread_count == 0:
            print(Cl.hek+' Progress: %i/%i' % ( i + 1, len(found)))


def single(args):
    result = crack(args.hash)
    if result:
        print Cl.hek+'HASH FOUND ==> ',result
    else:
        print Cl.gagal+'HASH NOT FOUND IN MY DATABASES :('

if file:
    try:
        exploits(file)
    except KeyboardInterrupt:
        ih = raw_input(Cl.ask+'are u sure want to exit [y/n] ')
        if ih=='y' or ih=='Y':
            sys.exit()
        elif ih=='n' or ih=='N':
            exploits(file)
        else:
            sys.exit()
        with open('cracked-%s'%file.split('/')[-1],'w+') as f:
            for hashval,cracked in result.items():
                f.write(hashval+':'+cracked+'\n')
    print Cl.hek+'file saved ! %s'%(file.split('/')[-1])

elif args.hash:
    single(args)
